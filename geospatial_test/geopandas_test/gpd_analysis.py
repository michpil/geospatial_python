import geopandas as gpd
import matplotlib.pyplot as plt
from datetime import datetime

# # Start datetime
# start = datetime.now()

# Read data
airports = gpd.read_file(r'..\data\ne_10m_airports\ne_10m_airports.shp')
countries = gpd.read_file(r'..\data\ne_10m_admin_0_countries\ne_10m_admin_0_countries.shp')
roads = gpd.read_file(r'..\data\ne_10m_roads\ne_10m_roads.shp')

# Select polygon and write data
country_poland = countries[countries['NAME'] == 'Poland']
country_poland.to_file(r'out\country_poland.shp')

# Select points and write data
airports_poland = gpd.overlay(airports, country_poland)
airports_poland.to_file(r'out\airports_poland.shp')

# Select lines and write data
roads_poland = gpd.clip(roads, country_poland)
roads_poland.to_file(r'out\roads_poland.shp')

# Change CRS
country_poland_proj = country_poland.to_crs(crs='EPSG:2180')
roads_poland_proj = roads_poland.to_crs(crs='EPSG:2180')
airports_poland_proj = airports_poland.to_crs(crs='EPSG:2180')

# Add columns and calculate value
country_poland_proj['ROADS_LENGTH_KM'] = int(roads_poland_proj['geometry'].length.sum() / 1000)
country_poland_proj['APT_NUMBER'] = len(airports_poland.index)

# Write data
country_poland_proj.to_file(r'out\country_poland_proj.shp')
roads_poland_proj.to_file(r'out\roads_poland_proj.shp')
airports_poland_proj.to_file(r'out\airports_poland_proj.shp')

# Create buffer
airports_poland_proj_buff = airports_poland_proj
airports_poland_proj_buff['geometry'] = airports_poland_proj_buff.geometry.buffer(50000)
airports_poland_proj_buff.to_file(r'out\airports_poland_proj_buff.shp')

# Erase buffer
country_poland_proj_erased = gpd.overlay(country_poland_proj, airports_poland_proj_buff, how='difference')
country_poland_proj_erased.to_file(r'out\country_poland_proj_erased.shp')

# Map
axs = plt.axes()
x_axis = axs.axes.get_xaxis()
y_axis = axs.axes.get_yaxis()
x_axis.set_visible(False)
y_axis.set_visible(False)

roads = roads_poland_proj.plot(ax=axs)
country = country_poland_proj_erased.plot(ax=roads)
airports_poland_proj = gpd.read_file(r'out\airports_poland_proj.shp')
airports = airports_poland_proj.plot(ax=roads)

plt.savefig("map.png")
plt.show()

# # Stop datetime and print time
# stop = datetime.now()
# print(stop - start)
