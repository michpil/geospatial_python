import os
# from PyQt5.QtWidgets import QAction, QMessageBox
from qgis.PyQt.QtGui import *
from qgis.PyQt.QtWidgets import *


class PluginCreator:
    def __init__(self, iface):
        self.iface = iface

    def initGui(self):
        self.action = QAction(QIcon(r'C:\repos\geospatial_python\geospatial_test\pyqgis_test\plugin\test_plugin\img\icon.png'), 'Test Plugin', self.iface.mainWindow())
        self.action.triggered.connect(self.run)
        self.iface.addToolBarIcon(self.action)

    def unload(self):
        self.iface.removeToolBarIcon(self.action)
        del self.action

    def run(self):
        QMessageBox.information(None, 'Test Plugin', 'Hello my friend it is my first plugin')