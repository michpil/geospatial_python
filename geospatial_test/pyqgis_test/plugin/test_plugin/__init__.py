
def classFactory(iface):
    from .plugin_creator import PluginCreator
    return PluginCreator(iface)
