@echo off
call "C:\QGIS3.30.0\bin\o4w_env.bat"
call "C:\QGIS3.30.0\bin\qt5_env.bat"
call "C:\QGIS3.30.0\bin\py3_env.bat"
 
@echo on
pyrcc5 -o resources.py resources.qrc

copy C:\repos\geospatial_python\geospatial_test\pyqgis_test\plugin\data_validation C:\Users\ox880e\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins\data_validation

pause