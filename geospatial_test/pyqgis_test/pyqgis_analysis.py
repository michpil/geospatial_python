from qgis.core import *
from PyQt5.QtGui import QGuiApplication
from qgis.analysis import QgsNativeAlgorithms
import processing
from processing.core.Processing import Processing
from PyQt5.QtCore import QVariant
import os
from qgis.core import (
    QgsMapSettings,
    QgsMapRendererParallelJob,
)
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtCore import QSize
from datetime import datetime

# # Start datetime
# start = datetime.now()

# Load geotools
Processing.initialize()
QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())

# Init QGIS app
app = QGuiApplication([])
QgsApplication.setPrefixPath(r"C:\OSGeo4W\apps\qgis", True)
QgsApplication.initQgis()

# Read data
countries = QgsVectorLayer(r'..\data\ne_10m_admin_0_countries\ne_10m_admin_0_countries.shp')

# Select polygon and write data
countries.selectByExpression('"NAME"=\'Poland\'')
QgsVectorFileWriter.writeAsVectorFormat(onlySelected=True, layer=countries, fileName=r'out\country_poland', driverName="ESRI Shapefile", fileEncoding="UTF-8")

# Select points and write data
airports = r'..\data\ne_10m_airports\ne_10m_airports.shp'
country = r'out\country_poland.shp'
out = r'out\airports_poland.shp'

processing.run("qgis:intersection", {'INPUT': airports, 'OVERLAY': country, 'OUTPUT': out})

# Select lines and write data
roads = r'..\data\ne_10m_roads\ne_10m_roads.shp'
country = r'out\country_poland.shp'
out = r'out\roads_poland.shp'

processing.run("qgis:clip", {'INPUT': roads, 'OVERLAY': country, 'OUTPUT': out})

# Change CRS ad write data
parameter = {'INPUT': r'out\roads_poland.shp', 'TARGET_CRS': 'EPSG:2180',
                 'OUTPUT': r'out\roads_poland_proj.shp'}
processing.run('qgis:reprojectlayer', parameter)

parameter = {'INPUT': r'out\country_poland.shp', 'TARGET_CRS': 'EPSG:2180',
                 'OUTPUT': r'out\country_poland_proj.shp'}
processing.run('qgis:reprojectlayer', parameter)

parameter = {'INPUT': r'out\airports_poland.shp', 'TARGET_CRS': 'EPSG:2180',
                 'OUTPUT': r'out\airports_poland_proj.shp'}
processing.run('qgis:reprojectlayer', parameter)

# Add columns and calculate value
country_poland_proj = QgsVectorLayer(r'out\country_poland_proj.shp')
layer_provider = country_poland_proj.dataProvider()
layer_provider.addAttributes([QgsField("ROADS_LENGTH_KM", QVariant.Int)])
layer_provider.addAttributes([QgsField("APT_NUMBER", QVariant.Int)])
country_poland_proj.updateFields()


roads_poland_proj = QgsVectorLayer(r'out\roads_poland_proj.shp')
features = roads_poland_proj.getFeatures()

summed_total = 0
for feat in features:
    length = feat.geometry().length()
    summed_total += length

airports_poland_proj = QgsVectorLayer(r'out\airports_poland_proj.shp')
count = len([feat for feat in airports_poland_proj.getFeatures()])


with edit(country_poland_proj):
    for feat in country_poland_proj.getFeatures():
        feat['ROADS_LENG'] = int(summed_total) / 1000
        feat['APT_NUMBER'] = count
        country_poland_proj.updateFeature(feat)

# Create buffer
processing.run("qgis:buffer",
    {'INPUT': r'out\airports_poland_proj.shp', 'DISTANCE': 50000, 'SEGMENTS': 5, 'END_CAP_STYLE': 0, 'JOIN_STYLE': 0, 'MITER_LIMIT': 2,
    'DISSOLVE': False, 'OUTPUT': r'out\airports_poland_proj_buff.shp'})

# Erase buffer
processing.run("qgis:difference", {'INPUT': r'out\country_poland_proj.shp', 'OVERLAY': r"out\airports_poland_proj_buff.shp",
                             'OUTPUT': r"out\country_poland_proj_erased.shp"})

# Map
country_poland_proj_erased = QgsVectorLayer(r'out\country_poland_proj_erased.shp')
settings = QgsMapSettings()
settings.setLayers([airports_poland_proj, roads_poland_proj, country_poland_proj_erased])
settings.setBackgroundColor(QColor(255, 255, 255))
settings.setOutputSize(QSize(800, 600))
settings.setExtent(country_poland_proj.extent())

render = QgsMapRendererParallelJob(settings)

def finished():
    img = render.renderedImage()
    img.save('map.png', "png")

render.finished.connect(finished)
render.start()

from qgis.PyQt.QtCore import QEventLoop
loop = QEventLoop()
render.finished.connect(loop.quit)
loop.exec_()

# # Stop datetime and print time
# stop = datetime.now()
# print(stop - start)
