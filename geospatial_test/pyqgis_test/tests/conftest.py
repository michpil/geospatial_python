from datetime import datetime
import getpass
import platform
import pytest
import struct


def pytest_configure(config):
    config._metadata["Datetime"] = datetime.now()
    config._metadata["Designer"] = 'QGIS Tester'
    config._metadata["Test Type"] = 'Automated'
    config._metadata["Tester"] = getpass.getuser()
    config._metadata["Pytest"] = pytest.__version__
    config._metadata["Python Version"] = f'(Python {platform.python_version()} on win{struct.calcsize("P") * 8})'
    config._metadata["System Version"] = platform.platform()
    config._metadata["Environment"] = 'TEST'
