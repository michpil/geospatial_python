import os
import sys

from qgis.core import QgsVectorLayer
from qgis.core import (
    QgsRasterLayer,
    QgsRasterBandStats
)
from qgis.analysis import QgsNativeAlgorithms
from qgis.core import QgsApplication

from map_data import create_map


input_vector_act_file = r"data/great_polish_sea.geojson"
vlayer_act = QgsVectorLayer(input_vector_act_file, "vector layer", "ogr")

input_vector_exp_file = r"data/world_lands.geojson"
vlayer_exp = QgsVectorLayer(input_vector_exp_file, "vector layer", "ogr")

# input_raster_file = r"data\srtm_germany_dsm\srtm_germany_dsm.tif"
# rlayer = QgsRasterLayer(input_raster_file, "elevation", "gdal")


class ActualVectorMetaData:
    encoding = vlayer_act.dataProvider().encoding()
    crs = vlayer_act.crs().authid()
    layer_type = str(vlayer_act.type())
    layer_name = vlayer_act.name()
    extent = {'MIN_LON': vlayer_act.extent().xMinimum(), 'MIN_LAT': vlayer_act.extent().yMinimum(), 'MAX_LON': vlayer_act.extent().xMaximum(), 'MAX_LAT': vlayer_act.extent().yMaximum()}
    geometry_type = str(vlayer_act.geometryType())
    storage_type = vlayer_act.storageType()
    fields_number = len(vlayer_act.fields())
    fields_name = {field.name(): field.name() for field in vlayer_act.fields()}
    fields_type_name = {field.name(): field.typeName() for field in vlayer_act.fields()}
    fields_type = {field.name(): field.type() for field in vlayer_act.fields()}
    fields_length = {field.name(): field.length() for field in vlayer_act.fields()}
    fields_precision = {field.name(): field.precision() for field in vlayer_act.fields()}
    fields_isreadonly = {field.name(): field.isReadOnly() for field in vlayer_act.fields()}
    fields_comment = {field.name(): field.comment() for field in vlayer_act.fields()}
    rows_number = vlayer_act.featureCount()


class ActualVectorDataAttributes:
    fields_name = [field.name() for field in vlayer_act.fields()]
    rows_data = [feature.attributes() for feature in vlayer_act.getFeatures()]
    data_attributes = {}
    for count, field_name in enumerate(fields_name):
        attribs = []
        for row in rows_data:
            attribs.append(row[count])
        data_attributes[field_name] = attribs


class DataProcessing:
    QgsApplication.setPrefixPath(r"C:\Program Files\QGIS3.30.0\apps\qgis", True)
    qgs = QgsApplication([], False)
    qgs.initQgis()
    sys.path.append(os.path.join(QgsApplication.prefixPath(), "python", "plugins"))
    import processing
    from processing.core.Processing import Processing
    Processing.initialize()
    QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())

    out_diff_filepath = r"data\diff_actual_expected.geojson"

    processing.run("qgis:symmetricaldifference",
                   {'INPUT': input_vector_act_file, 'OVERLAY': input_vector_exp_file,
                    'OUTPUT': out_diff_filepath})

    vlayer_diff_act_exp = QgsVectorLayer(out_diff_filepath, "vector layer", "ogr")

    vlayer_diff_act_exp_row_count = vlayer_diff_act_exp.featureCount()
    vlayer_diff_act_exp_isempty = True if vlayer_diff_act_exp_row_count == 0 else False

    create_map(input_vector_act_file, input_vector_exp_file, out_diff_filepath,
               r'logs\map_diff.html')

    qgs.exitQgis()


# class ActualRasterMetaData:
#     crs = rlayer.crs().authid()
#     layer_type = str(rlayer.type())
#     layer_name = rlayer.name()
#     extent = {'MIN_LON': rlayer.extent().xMinimum(), 'MIN_LAT': rlayer.extent().yMinimum(), 'MAX_LON': rlayer.extent().xMaximum(), 'MAX_LAT': rlayer.extent().yMaximum()}
#     raster_size = {'WIDTH': rlayer.width(), 'HEIGHT': rlayer.height()}
#     pixel_size = {'X': rlayer.rasterUnitsPerPixelX(), 'Y': rlayer.rasterUnitsPerPixelY()}
#     bands_count = rlayer.bandCount()
#     bands_name = []
#     for band_count in range(1, rlayer.bandCount()+1):
#         bands_name.append(rlayer.bandName(band_count))
#     band1_mean_value = rlayer.dataProvider().bandStatistics(1, QgsRasterBandStats.All).mean
#     band1_min_value = rlayer.dataProvider().bandStatistics(1, QgsRasterBandStats.All).minimumValue
#     band1_max_value = rlayer.dataProvider().bandStatistics(1, QgsRasterBandStats.All).maximumValue
