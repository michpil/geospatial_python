class ExpectedVectorMetaData:
    encoding = 'UTF-8'
    crs = 'EPSG:4326'
    extent = {'MAX_LAT': 83.63410065300007545, 'MAX_LON': 181, 'MIN_LAT': -90, 'MIN_LON': -181}
    fields_comment = {'featurecla': '', 'min_zoom': '', 'scalerank': ''}
    fields_isreadonly = {'featurecla': False, 'min_zoom': False, 'scalerank': False}
    fields_length = {'featurecla': 0, 'min_zoom': 0, 'scalerank': 0}
    fields_name = {'featurecla': 'featurecla', 'min_zoom': 'min_zoom', 'scalerank': 'scalerank'}
    fields_number = 3
    fields_precision = {'featurecla': 0, 'min_zoom': 0, 'scalerank': 0}
    fields_type = {'featurecla': 10, 'min_zoom': 6, 'scalerank': 2}
    fields_type_name = {'featurecla': 'String', 'min_zoom': 'Real', 'scalerank': 'Integer'}
    geometry_type = 'GeometryType.Polygon'
    layer_name = 'vector layer'
    rows_number = 10
    storage_type = 'GeoJSON'
    layer_type = 'LayerType.Vector'


class ExpectedVectorDataAttributes:
    field_min_zoom_range = 0, 3
