from folium.plugins import MousePosition, FloatImage
import folium


def create_map(input_data_actual, input_data_expected, diff_act_exp,  output_filepath):
    m = folium.Map(
        tiles="OpenStreetMap",
        zoom_start=2, control_scale=True
    )

    def style_function_actual(feature):
        return {
            "fillOpacity": 0.5,
            "weight": 2,
            "color": 'green',
            "fillColor": "green"
        }

    def style_function_expected(feature):
        return {
            "fillOpacity": 0.5,
            "weight": 2,
            "color": 'blue',
            "fillColor": "blue"
        }

    def style_function_diff_act_exp(feature):
        return {
            "fillOpacity": 0.5,
            "weight": 2,
            "color": 'red',
            "fillColor": "red"
        }

    folium.GeoJson(input_data_actual, name="actual", style_function=style_function_actual).add_to(m)
    folium.GeoJson(input_data_expected, name="expected", style_function=style_function_expected).add_to(m)
    folium.GeoJson(diff_act_exp, name="diff_actual_expected", style_function=style_function_diff_act_exp).add_to(m)


    folium.TileLayer(tiles='Stamen Terrain').add_to(m)
    folium.TileLayer(tiles='Stamen Toner').add_to(m)
    folium.TileLayer(tiles='Stamen Watercolor').add_to(m)
    folium.TileLayer(tiles='CartoDB dark_matter').add_to(m)
    folium.LayerControl().add_to(m)
    FloatImage(r'north_arrow.jpg', bottom=100, left=0, position='relative').add_to(m)

    formatter_lat = "function(num) {return 'LAT : ' + L.Util.formatNum(num, 4) + ' ';};"
    formatter_lon = "function(num) {return 'LON : ' + L.Util.formatNum(num, 4) + ' ';};"

    MousePosition(
        position="bottomleft",
        separator=" | ",
        empty_string="NaN",
        lng_first=True,
        num_digits=20,
        prefix='Coordinates | ',
        lat_formatter=formatter_lat,
        lng_formatter=formatter_lon,
    ).add_to(m)

    m.save(output_filepath)
