from actual_data import ActualVectorMetaData as act_v_md
from actual_data import ActualVectorDataAttributes as act_v_d
from expected_data import ExpectedVectorMetaData as exp_v_md
from expected_data import ExpectedVectorDataAttributes as exp_v_d
from actual_data import DataProcessing as dp
import pytest


test_failed_message = 'Wrong Actual vs Expected'


class TestVectorMetaData:
    def test_metadatadata_crs(self):
        assert act_v_md.crs == exp_v_md.crs, test_failed_message

    @pytest.mark.parametrize('actual, expected', [(act_v_md.extent.get(key), expected) for key, expected in exp_v_md.extent.items()], ids=list(exp_v_md.extent.keys()))
    def test_metadatadata_extent_1(self, actual, expected):
        assert actual == expected, test_failed_message

    def test_metadatadata_extent_2(self):
        actual_expected = [(key, act_v_md.extent.get(key), expected) for key, expected in exp_v_md.extent.items()]
        failed = []
        for key, actual, expected in actual_expected:
            if actual != expected:
                failed.append(f'\nATTRIB: {key} | ACTUAL: {actual} | EXPECTED: {expected}')
        assert len(failed) == 0, f'{test_failed_message} ({len(failed)}) {"".join(failed)}'


class TestVectorDataAttributes:
    def test_attrib_min_zoom_range(self):
        failed = []
        for id, actual in enumerate(act_v_d.data_attributes.get('min_zoom'), 1):
            range_min = exp_v_d.field_min_zoom_range[0]
            range_max = exp_v_d.field_min_zoom_range[1]
            if not range_min <= actual <= range_max:
                failed.append(f'\nID: {id} | ACTUAL: {actual} | EXPECTED: in range {range_min} - {range_max}')
        assert len(failed) == 0, f'{test_failed_message} ({len(failed)}) {"".join(failed)}'


class TestVectorDataGeometry:
    def test_geometry_match(self):
        assert dp.vlayer_diff_act_exp_isempty is True, test_failed_message
