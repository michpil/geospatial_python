@echo off
SET OSGEO4W_ROOT=C:\PROGRA~1\QGIS3~3.30.0
call "%OSGEO4W_ROOT%"\bin\o4w_env.bat
call "%OSGEO4W_ROOT%"\apps\grass\grass78\etc\env.bat
@echo off
path %PATH%;%OSGEO4W_ROOT%\apps\qgis\bin
path %PATH%;%OSGEO4W_ROOT%\apps\grass\grass78\lib
path %PATH%;C:\PROGRA~1\QGIS3~3.30.0\apps\Qt5\bin
path %PATH%;C:\PROGRA~1\QGIS3~3.30.0\apps\Python39\Scripts

set PYTHONPATH=%PYTHONPATH%;%OSGEO4W_ROOT%\apps\qgis\python
set PYTHONHOME=%OSGEO4W_ROOT%\apps\Python39

set PATH=C:\Program Files\Git\bin;%PATH%

cmd.exe