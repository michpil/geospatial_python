import arcpy
from arcpy import env
from datetime import datetime

# # Start datetime
# start = datetime.now()

# Create workspace
out = "C:/Users/michp/PycharmProjects/GeospatialAnalysis/geospatial_test/arcpy_test/out"
env.workspace = out
env.overwriteOutput = True

# Read data
airports = r'..\data\ne_10m_airports\ne_10m_airports.shp'
countries = r'..\data\ne_10m_admin_0_countries\ne_10m_admin_0_countries.shp'
roads = r'..\data\ne_10m_roads\ne_10m_roads.shp'

# Select polygon and write data
arcpy.MakeFeatureLayer_management(countries, "country_poland")
arcpy.SelectLayerByAttribute_management("country_poland", 'NEW_SELECTION', '"NAME"=\'Poland\'')
arcpy.FeatureClassToShapefile_conversion("country_poland", out)

# Select points and write data
arcpy.MakeFeatureLayer_management(airports, "airports_poland")
arcpy.SelectLayerByLocation_management('airports_poland', 'intersect', 'country_poland')
arcpy.FeatureClassToShapefile_conversion("airports_poland", out)

# Select lines and write data
arcpy.Clip_analysis(roads, "country_poland.shp", r"roads_poland.shp")

# Change CRS and write data
proj = arcpy.SpatialReference("C:/Users/michp/PycharmProjects/GeospatialAnalysis/geospatial_test/arcpy_test/epsg2180.prj")
arcpy.Project_management(r"country_poland.shp", r"country_poland_proj.shp", proj)
arcpy.Project_management(r"roads_poland.shp", r"roads_poland_proj.shp", proj)
arcpy.Project_management(r"airports_poland.shp", r"airports_poland_proj.shp", proj)

# Add columns and calculate value
arcpy.AddField_management("country_poland_proj.shp", 'ROADS_LENG')
arcpy.AddField_management("country_poland_proj.shp", 'APT_NUMBER')

with arcpy.da.SearchCursor("roads_poland_proj.shp", "SHAPE@LENGTH") as cursor:
    summed_total = 0
    for row in cursor:
        summed_total += row[0]

count = arcpy.GetCount_management("airports_poland.shp").getOutput(0)

arcpy.CalculateField_management("country_poland_proj.dbf", 'ROADS_LENG', summed_total / 1000, "PYTHON_9.3")
arcpy.CalculateField_management("country_poland_proj.dbf", 'APT_NUMBER', count, "PYTHON_9.3")

# Create buffer
arcpy.Buffer_analysis("airports_poland_proj.shp", "airports_poland_proj_buff.shp", '50000 Meters')

# Erase buffer
arcpy.Erase_analysis("country_poland_proj.shp", "airports_poland_proj_buff.shp", "country_poland_proj_erased.shp")

# Map arcgis desktop
mxd = arcpy.mapping.MapDocument(r"project.mxd")
df = arcpy.mapping.ListDataFrames(mxd)[0]
layers = arcpy.mapping.ListLayers(mxd, '', df)
for layer in layers:
    arcpy.mapping.RemoveLayer(df, layer)

newlayer = arcpy.mapping.Layer(r'out/country_poland_proj_erased.shp')
arcpy.mapping.AddLayer(df, newlayer, "BOTTOM")
newlayer = arcpy.mapping.Layer(r'out/airports_poland_proj.shp')
arcpy.mapping.AddLayer(df, newlayer, "BOTTOM")
newlayer = arcpy.mapping.Layer(r'out/roads_poland_proj.shp')
arcpy.mapping.AddLayer(df, newlayer, "BOTTOM")

mxd.save()
arcpy.mapping.ExportToPNG(mxd, 'map.png', df_export_width=800, df_export_height=600)
del mxd, df, newlayer

# # Map arcgis pro
# # arcpy.MakeFeatureLayer_management(r"out/airports_poland_proj.shp", r"airports_poland_proj1")
# arcpy.SaveToLayerFile_management(r"C:/Users/michp/PycharmProjects/GeospatialAnalysis/geospatial_test/arcpy_test/out/airports_poland_proj.shp", r"out/airports_poland_proj")
# aprx = arcpy.mp.ArcGISProject("project.aprx")
# m = aprx.listMaps("Map")[0]
#
# m.addLayer(r"out/airports_poland_proj.lyr")
# lyt = aprx.listLayouts("Layout")[0]
# lyt = aprx.listLayouts()[0]
# lyt.exportToPNG('map1.png')


# # Stop datetime and print time
# stop = datetime.now()
# print(stop - start)
